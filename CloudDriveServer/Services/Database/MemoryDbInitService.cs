using CloudDriveServer.Interfaces.Database;

namespace CloudDriveServer.Services.Database;

public class MemoryDbInitService : IDbInitService
{
  private readonly IDbUserManagement _userManagement;

  public MemoryDbInitService(IDbUserManagement userManagement)
  {
    _userManagement = userManagement;
  }

  public async Task InitDatabase()
  {
    await _userManagement.InitEmptyDb();
  }
}
