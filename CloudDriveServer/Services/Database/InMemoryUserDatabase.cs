using CloudDriveServer.Configuration;
using CloudDriveServer.Interfaces.Database;
using CloudDriveServer.Models.Database.Responses;
using CloudDriveServer.Models.UserManagement;
using CloudDriveServer.Models.UserManagement.Responses;
using Microsoft.Extensions.Options;
using Serilog;

namespace CloudDriveServer.Services.Database;

public class InMemoryUserDatabase : IDbUserManagement
{
  private readonly StorageConfiguration _storageConfiguration;

  private readonly Dictionary<string, User> _users = new();
  /*
  {
      {
          "admin", new User(
              DateTime.Now,
              DateTime.Now,
              "admin",
              "admin@admin.com",
              "qsqELPLe/TpOEM8nNG3EciorHiCM7QwX7Q8NZgiZyDD+ekZ1OWGYrdx2cGMKMnDT/lm2ingx0Y0KL5Jj4eusyg==",
              "PSXEE/SkQx2h7GGN4QzpmxsJNHCvZoHPdrS+AnQ0zk0=",
              1000,
              Role.Owner)
      }
  };
  */

  public InMemoryUserDatabase(IOptions<StorageConfiguration> storageConfiguration)
  {
    _storageConfiguration = storageConfiguration.Value;

    if (string.IsNullOrEmpty(_storageConfiguration.BasePath))
    {
      throw new ArgumentNullException(nameof(_storageConfiguration.BasePath), "storage base path is empty");
    }
  }

  public Task InitEmptyDb()
  {
    if (_users.Any()) return Task.CompletedTask;

    _users.Add("admin", new User(
        DateTime.Now,
        DateTime.Now,
        "admin",
        "admin@admin.com",
        "qsqELPLe/TpOEM8nNG3EciorHiCM7QwX7Q8NZgiZyDD+ekZ1OWGYrdx2cGMKMnDT/lm2ingx0Y0KL5Jj4eusyg==",
        "PSXEE/SkQx2h7GGN4QzpmxsJNHCvZoHPdrS+AnQ0zk0=",
        1000,
        Role.Owner));

    var adminHomeDirectory = Path.Join(_storageConfiguration.BasePath, "admin");

    if (Directory.Exists(adminHomeDirectory)) return Task.CompletedTask;

    try
    {
      Directory.CreateDirectory(adminHomeDirectory);
    }
    catch (Exception ex)
    {
      Log.Error(ex, "could not create admin directory because of {Ex}", ex);
    }

    return Task.CompletedTask;
  }

  public Task<CreateUserResponse> CreateUser(User user)
  {
    if (string.IsNullOrEmpty(user.Name) || string.IsNullOrEmpty(user.PasswordHash))
    {
      return Task.FromResult(new CreateUserResponse
      {
        Status = CreateUserResponseStatus.UsernameNotProvided,
        User = null
      });
    }

    var success = _users.TryAdd(user.Name, user);
    if (!success)
    {
      return Task.FromResult(new CreateUserResponse
      {
        Status = CreateUserResponseStatus.UserAlreadyExists,
        User = null,
      });
    }

    return Task.FromResult(new CreateUserResponse
    {
      Status = CreateUserResponseStatus.Ok,
      User = user
    });
  }

  public Task<EditUserResponse> UpdateUser(User user)
  {
    if (string.IsNullOrEmpty(user.Name))
    {
      return Task.FromResult(new EditUserResponse
      {
        Status = EditUserResponseStatus.UserNotFound,
        User = null
      });
    }

    var exists = _users.ContainsKey(user.Name);
    if (!exists)
    {
      return Task.FromResult(new EditUserResponse
      {
        Status = EditUserResponseStatus.UserNotFound,
        User = null
      });
    }

    _users[user.Name] = user;
    return Task.FromResult(new EditUserResponse
    {
      Status = EditUserResponseStatus.Ok,
      User = user
    });
  }

  public Task<DeleteUserResponse> DeleteUser(string username)
  {
    if (string.IsNullOrEmpty(username))
    {
      return Task.FromResult(new DeleteUserResponse
      {
        Status = DeleteUserResponseStatus.UserNotFound,
      });
    }

    var success = _users.Remove(username);
    if (!success)
    {
      return Task.FromResult(new DeleteUserResponse
      {
        Status = DeleteUserResponseStatus.UserNotFound
      });
    }

    return Task.FromResult(new DeleteUserResponse
    {
      Status = DeleteUserResponseStatus.Ok
    });
  }

  public Task<GetUserResponse> GetUser(string username)
  {
    if (string.IsNullOrEmpty(username))
    {
      return Task.FromResult(new GetUserResponse
      {
        Status = GetUserResponseStatus.UserNotFound,
        User = null
      });
    }

    var exists = _users.TryGetValue(username, out var user);
    if (!exists)
    {
      return Task.FromResult(new GetUserResponse
      {
        Status = GetUserResponseStatus.UserNotFound,
        User = null
      });
    }

    return Task.FromResult(new GetUserResponse
    {
      Status = GetUserResponseStatus.Ok,
      User = user
    });
  }

  public Task<List<User>> ListUsers()
  {
    var users = _users.Values;

    var result = users.ToList();

    return Task.FromResult(result);
  }

  public Task<AddRefreshTokenResponse> AddRefreshTokenToUser(string username, RefreshToken refreshToken)
  {
    var success = _users.TryGetValue(username, out var user);
    if (!success || user is null)
      return Task.FromResult(new AddRefreshTokenResponse { Status = AddRefreshTokenResponseStatus.UserNotFound });

    user.RefreshTokens.Add(refreshToken);
    return Task.FromResult(new AddRefreshTokenResponse { Status = AddRefreshTokenResponseStatus.Ok });
  }

  public Task<CheckRefreshTokenResponse> CheckRefreshToken(string username, string token)
  {
    var success = _users.TryGetValue(username, out var user);
    if (!success || user is null)
      return Task.FromResult(
          new CheckRefreshTokenResponse { Status = CheckRefreshTokenResponseStatus.UserNotFound });

    var refreshToken = user.RefreshTokens.FirstOrDefault(x => x.Token == token);

    if (refreshToken is null || string.IsNullOrEmpty(refreshToken.Token))
      return Task.FromResult(new CheckRefreshTokenResponse
      { Status = CheckRefreshTokenResponseStatus.TokenNotFound });

    return Task.FromResult(DateTime.UtcNow > refreshToken.ValidTo
        ? new CheckRefreshTokenResponse { Status = CheckRefreshTokenResponseStatus.TokenExpired }
        : new CheckRefreshTokenResponse { Status = CheckRefreshTokenResponseStatus.Ok });
  }

  public Task SetLastSignInOnForUser(string username, DateTime lastSignIn)
  {
    var success = _users.TryGetValue(username, out var user);
    if (!success || user is null)
      return Task.CompletedTask;

    user.LastSignInOn = lastSignIn;
    return Task.CompletedTask;
  }
}
