using System.IO.Compression;
using CloudDriveServer.Configuration;
using CloudDriveServer.Interfaces;
using CloudDriveServer.Models.Files;
using CloudDriveServer.Models.Files.Requests;
using CloudDriveServer.Models.Files.Responses;
using Microsoft.Extensions.Options;

namespace CloudDriveServer.Services;

public class FileService : IFileService
{
  private readonly ILogger<FileService> _logger;
  private readonly StorageConfiguration _storageConfiguration;

  public FileService(ILogger<FileService> logger,
    IOptions<StorageConfiguration> storageConfiguration)
  {
    _logger = logger;
    _storageConfiguration = storageConfiguration.Value;
  }

  private static bool IsPathValid(string path)
  {
    // no relative paths
    if (path.Contains(".."))
      return false;

    try
    {
      var _ = new FileInfo(path);
    }
    catch (Exception)
    {
      return false;
    }

    return true;
  }

  public ListFilesResponse ListFiles(ListFilesRequest request)
  {
    if (string.IsNullOrEmpty(request.Username))
      return new ListFilesResponse {Status = ListFilesResponseStatus.NoUserProvided};

    var validPath = IsPathValid(request.Path);
    if (!validPath)
      return new ListFilesResponse {Status = ListFilesResponseStatus.PathInvalid};

    var fullPath = Path.Join(_storageConfiguration.BasePath, request.Username, request.Path);
    var exists = Directory.Exists(fullPath);
    if (!exists)
      return new ListFilesResponse {Status = ListFilesResponseStatus.DirectoryDoesNotExist};

    var directoryContents = new List<FileDto>();

    try
    {
      var directoryInfo = new DirectoryInfo(fullPath);
      var directories = directoryInfo.GetDirectories();
      var files = directoryInfo.GetFiles();

      directoryContents.AddRange(from directory in directories
        let name = directory.Name
        let lastModifiedOn = directory.LastWriteTime
        let filesInDirectory = directory.GetFiles().Length
        let directoriesInDirectory = directory.GetDirectories().Length
        let size = (uint) (filesInDirectory + directoriesInDirectory)
        select new FileDto
        {
          Name = name, Type = FileType.Directory, Size = size, LastModifiedOn = lastModifiedOn
        });

      directoryContents.AddRange(from file in files
        let name = file.Name
        let lastModifiedOn = file.LastWriteTime
        let size = (uint) file.Length
        select new FileDto
        {
          Name = name, Type = FileType.File, Size = size, LastModifiedOn = lastModifiedOn
        });
    }
    catch (Exception ex)
    {
      _logger.LogError(ex, "could not list files for user {User} in directory {Path} due to {Ex}",
        request.Username, request.Path, ex);
      return new ListFilesResponse
      {
        Status = ListFilesResponseStatus.ListingError, Message = ex.Message
      };
    }

    return new ListFilesResponse
    {
      Status = ListFilesResponseStatus.Ok, Contents = directoryContents
    };
  }

  public MakeDirectoryResponse MakeDirectory(MakeDirectoryRequest request)
  {
    if (string.IsNullOrEmpty(request.Username))
      return new MakeDirectoryResponse {Status = MakeDirectoryResponseStatus.NoUserProvided};

    var valid = IsPathValid(request.Path);
    if (!valid)
      return new MakeDirectoryResponse {Status = MakeDirectoryResponseStatus.PathInvalid};

    var fullPath = Path.Join(_storageConfiguration.BasePath, request.Username, request.Path);

    try
    {
      var directoryInfo = Directory.CreateDirectory(fullPath);
      _logger.LogInformation("created directory {Path} for user {User}", directoryInfo.FullName,
        request.Username);
      return new MakeDirectoryResponse {Status = MakeDirectoryResponseStatus.Ok};
    }
    catch (Exception ex)
    {
      _logger.LogError(ex, "could not create directory {Path} for user {User} due to {Ex}",
        request.Path,
        request.Username, ex);
      return new MakeDirectoryResponse
      {
        Status = MakeDirectoryResponseStatus.CreationFailed, Message = ex.Message
      };
    }
  }

  public async Task<UploadFileResponse> UploadFiles(UploadFileRequest request)
  {
    if (string.IsNullOrEmpty(request.Username))
      return new UploadFileResponse {Status = UploadFileResponseStatus.NoUserProvided};

    if (!request.Files.Any())
      return new UploadFileResponse {Status = UploadFileResponseStatus.NoFilesProvided};

    var result = new UploadFileResponse {Status = UploadFileResponseStatus.Ok};

    foreach (var file in request.Files)
    {
      if (file.Length == 0)
        continue;

      var pathToUpload = file.Name;

      var isValid = IsPathValid(pathToUpload);
      if (!isValid)
        return new UploadFileResponse {Status = UploadFileResponseStatus.FilenameInvalid};

      try
      {
        var filename = Path.GetFileName(file.FileName);
        if (string.IsNullOrEmpty(filename))
          return new UploadFileResponse {Status = UploadFileResponseStatus.FilenameInvalid};

        var fullPath = Path.Join(_storageConfiguration.BasePath, request.Username, pathToUpload,
          filename);
        await using var stream = new FileStream(fullPath, FileMode.Create);
        await file.CopyToAsync(stream);
        result.UploadedFiles.Add(Path.Join(pathToUpload, filename));
      }
      catch (Exception ex)
      {
        _logger.LogError(ex, "could not save file for user {User} due to {Ex}", request.Username,
          ex);
        return new UploadFileResponse
        {
          Status = UploadFileResponseStatus.UploadFailed, Message = ex.Message
        };
      }
    }

    return result;
  }

  public DownloadFileResponse DownloadFile(DownloadFileRequest request)
  {
    if (string.IsNullOrEmpty(request.Username))
      return new DownloadFileResponse {Status = DownloadFileResponseStatus.NoUserProvided};

    if (string.IsNullOrEmpty(request.Path))
      return new DownloadFileResponse {Status = DownloadFileResponseStatus.FilenameInvalid};

    var isValidPath = IsPathValid(request.Path);
    if (!isValidPath)
      return new DownloadFileResponse {Status = DownloadFileResponseStatus.FilenameInvalid};

    var absolutePath = Path.Join(_storageConfiguration.BasePath, request.Username, request.Path);

    bool isDirectory;
    try
    {
      var fileAttributes = File.GetAttributes(absolutePath);
      isDirectory = (fileAttributes & FileAttributes.Directory) == FileAttributes.Directory;
    }
    catch (Exception)
    {
      return new DownloadFileResponse {Status = DownloadFileResponseStatus.FileNotFound};
    }

    switch (isDirectory)
    {
      case true:
        var archiveName = absolutePath + ".zip";
        if (File.Exists(archiveName))
          File.Delete(archiveName);
        ZipFile.CreateFromDirectory(absolutePath, archiveName);
        return new DownloadFileResponse
        {
          Status = DownloadFileResponseStatus.OkArchive, FilePath = archiveName
        };

      case false:
        return new DownloadFileResponse
        {
          Status = DownloadFileResponseStatus.Ok, FilePath = absolutePath
        };
    }
  }
}
