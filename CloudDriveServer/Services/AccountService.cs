using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using CloudDriveServer.Configuration;
using CloudDriveServer.Interfaces;
using CloudDriveServer.Interfaces.Database;
using CloudDriveServer.Models.Database.Responses;
using CloudDriveServer.Models.UserManagement;
using CloudDriveServer.Models.UserManagement.Requests;
using CloudDriveServer.Models.UserManagement.Responses;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Serilog;

namespace CloudDriveServer.Services;

public class AccountService : IAccountService
{
  private readonly IDbUserManagement _dbUserManagement;
  private readonly JwtConfiguration _jwtConfiguration;
  private readonly StorageConfiguration _storageConfiguration;
  private readonly SymmetricSecurityKey _key;

  public AccountService(IOptions<JwtConfiguration> jwtConfig,
    IOptions<StorageConfiguration> storageConfiguration,
    IDbUserManagement dbUserManagement)
  {
    _dbUserManagement = dbUserManagement;
    _storageConfiguration = storageConfiguration.Value;
    if (string.IsNullOrEmpty(_storageConfiguration.BasePath))
      throw new ArgumentNullException(nameof(_storageConfiguration.BasePath),
        "storage base path is empty");
    _jwtConfiguration = jwtConfig.Value;
    if (string.IsNullOrEmpty(_jwtConfiguration.Secret))
      throw new ArgumentNullException(nameof(_jwtConfiguration.Secret), "Jwt secret is empty");
    var secretAsBytes = Encoding.UTF8.GetBytes(_jwtConfiguration.Secret);
    _key = new SymmetricSecurityKey(secretAsBytes);
  }

  private static string CreatePasswordHash(string password, string salt)
  {
    var passwordAndSalt = $"{password}{salt}";

    var shaManager = SHA512.Create();
    var passwordHashBytes = shaManager.ComputeHash(Encoding.UTF8.GetBytes(passwordAndSalt));
    var passwordHash = Convert.ToBase64String(passwordHashBytes);
    return passwordHash;
  }

  private string? CreateJwt(User user)
  {
    // generate claims
    var claims = new List<Claim>
    {
      new(ClaimTypes.Name, user.Name), new(ClaimTypes.Role, User.RoleToString(user.Role))
    };
    var credentials = new SigningCredentials(_key, SecurityAlgorithms.HmacSha256);
    var token = new JwtSecurityToken(
      claims: claims,
      expires: DateTime.Now.Add(TimeSpan.FromMinutes(_jwtConfiguration.ExpirationInMinutes)),
      audience: _jwtConfiguration.Audience,
      issuer: _jwtConfiguration.Issuer,
      signingCredentials: credentials
    );

    var result = new JwtSecurityTokenHandler().WriteToken(token);
    return result;
  }

  public async Task<CreateUserResponse> CreateUser(CreateUserRequest userRequest)
  {
    if (string.IsNullOrEmpty(userRequest.Username))
      return new CreateUserResponse {Status = CreateUserResponseStatus.UsernameNotProvided};
    if (string.IsNullOrEmpty(userRequest.Password))
      return new CreateUserResponse {Status = CreateUserResponseStatus.PasswordNotProvided};

    var usernameLower = userRequest.Username.ToLower();
    if (!IsAlphaNumeric(usernameLower))
      return new CreateUserResponse {Status = CreateUserResponseStatus.UsernameNotAlphaNumeric};


    var random = new Random();
    var saltBytes = new byte[32];
    random.NextBytes(saltBytes);
    var salt = Convert.ToBase64String(saltBytes);

    var passwordHash = CreatePasswordHash(userRequest.Password, salt);

    var user = new User
    (
      DateTime.Now,
      DateTime.Now,
      usernameLower,
      userRequest.Email,
      passwordHash,
      salt,
      userRequest.Quota ?? 10,
      User.StringToRole(userRequest.Role),
      new List<RefreshToken>()
    );

    var result = await _dbUserManagement.CreateUser(user);

    if (result.Status != CreateUserResponseStatus.Ok) return result;

    var (success, message) = CreateUserHomeDirectory(usernameLower);
    if (success) return result;

    result.Status = CreateUserResponseStatus.HomeDirectoryNotCreated;
    result.Message = message;

    return result;
  }

  private static bool IsAlphaNumeric(string input)
  {
    var rg = new Regex(@"^[a-zA-Z0-9\s,]*$");
    return rg.IsMatch(input);
  }

  private (bool, string?) CreateUserHomeDirectory(string username)
  {
    var userHomeDirectory = Path.Join(_storageConfiguration.BasePath, username);
    if (Directory.Exists(userHomeDirectory)) return (true, null);

    try
    {
      Directory.CreateDirectory(userHomeDirectory);
      return (true, null);
    }
    catch (Exception ex)
    {
      Log.Error(ex, "Could not create home directory for {User} due to {Ex}", username, ex);
      return (false, ex.ToString());
    }
  }

  public async Task<SignInResponse> SignIn(string username, string password)
  {
    if (string.IsNullOrEmpty(username) || string.IsNullOrEmpty(password))
      return new SignInResponse {Status = SignInResponseStatus.UsernameOrPasswordEmpty};

    var usernameLower = username.ToLower();
    var result = await _dbUserManagement.GetUser(username);

    switch (result.Status)
    {
      case GetUserResponseStatus.Ok:
        if (result.User is null)
          return new SignInResponse {Status = SignInResponseStatus.UserNotFound};
        break;
      case GetUserResponseStatus.UserNotFound:
        return new SignInResponse {Status = SignInResponseStatus.UserNotFound};
      case GetUserResponseStatus.UsernameEmpty:
        return new SignInResponse {Status = SignInResponseStatus.UsernameOrPasswordEmpty};
      default:
        throw new ArgumentOutOfRangeException(nameof(result.Status),
          "Database returned invalid status");
    }

    var passwordHash = CreatePasswordHash(password, result.User.Salt);

    if (result.User.PasswordHash != passwordHash)
      return new SignInResponse {Status = SignInResponseStatus.WrongPassword};

    var jwt = CreateJwt(result.User);
    var refreshToken = new RefreshToken
    {
      Token = Guid.NewGuid().ToString(), ValidTo = DateTime.Now.AddYears(1)
    };

    await _dbUserManagement.AddRefreshTokenToUser(usernameLower, refreshToken);
    await _dbUserManagement.SetLastSignInOnForUser(usernameLower, DateTime.Now);

    return new SignInResponse
    {
      Status = SignInResponseStatus.Ok, Jwt = jwt, RefreshToken = refreshToken.Token
    };
  }

  public async Task<RefreshSignInResponse> RefreshSignIn(string username, string refreshToken)
  {
    var result = await _dbUserManagement.CheckRefreshToken(username, refreshToken);
    switch (result.Status)
    {
      case CheckRefreshTokenResponseStatus.Ok:
        var userResponse = await _dbUserManagement.GetUser(username);
        if (userResponse.Status != GetUserResponseStatus.Ok || userResponse.User is null)
          return new RefreshSignInResponse {Status = RefreshSignInResponseStatus.UserNotFound};

        var jwt = CreateJwt(userResponse.User);
        return new RefreshSignInResponse
        {
          Status = RefreshSignInResponseStatus.Ok, Jwt = jwt, RefreshToken = refreshToken
        };

      case CheckRefreshTokenResponseStatus.UserNotFound:
        return new RefreshSignInResponse {Status = RefreshSignInResponseStatus.UserNotFound};
      case CheckRefreshTokenResponseStatus.TokenNotFound:
        return new RefreshSignInResponse {Status = RefreshSignInResponseStatus.TokenNotFound};
      case CheckRefreshTokenResponseStatus.TokenExpired:
        return new RefreshSignInResponse {Status = RefreshSignInResponseStatus.TokenExpired};
      default:
        return new RefreshSignInResponse {Status = RefreshSignInResponseStatus.TokenNotFound};
    }
  }

  public async Task<EditUserResponse> EditUser(EditUserRequest editUserRequest)
  {
    if (string.IsNullOrEmpty(editUserRequest.Name))
      return new EditUserResponse {Status = EditUserResponseStatus.NameEmpty};
    var userFromDb = await _dbUserManagement.GetUser(editUserRequest.Name);

    if (userFromDb.Status != GetUserResponseStatus.Ok || userFromDb.User is null)
      return new EditUserResponse {Status = EditUserResponseStatus.UserNotFound};

    var user = userFromDb.User;

    if (!string.IsNullOrEmpty(editUserRequest.Email))
      user.Email = editUserRequest.Email;

    if (editUserRequest.Quota is not null)
      user.Quota = editUserRequest.Quota.Value;

    if (!string.IsNullOrEmpty(editUserRequest.Role))
      user.Role = User.StringToRole(editUserRequest.Role);

    var updatedUser = await _dbUserManagement.UpdateUser(user);
    return updatedUser;
  }

  public async Task<DeleteUserResponse> DeleteUser(string username)
  {
    if (string.IsNullOrEmpty(username))
      return new DeleteUserResponse {Status = DeleteUserResponseStatus.UserNotFound};

    var deletedUser = await _dbUserManagement.DeleteUser(username);

    if (deletedUser.Status != DeleteUserResponseStatus.Ok) return deletedUser;

    var (success, message) = DeleteUserHomeDirectory(username);
    if (success) return deletedUser;

    deletedUser.Status = DeleteUserResponseStatus.CouldNotDeleteHomeDirectory;
    deletedUser.Message = message;

    return deletedUser;
  }

  private (bool, string?) DeleteUserHomeDirectory(string username)
  {
    var userHomeDirectory = Path.Join(_storageConfiguration.BasePath, username);

    if (!Directory.Exists(userHomeDirectory)) return (true, null);

    try
    {
      Directory.Delete(userHomeDirectory, true);
      return (true, null);
    }
    catch (Exception ex)
    {
      Log.Error(ex, "Could not clean up home directory of user {User} because of {Ex}", username,
        ex);
      return (false, ex.ToString());
    }
  }

  public async Task<GetUserDtoResponse> GetUser(string username)
  {
    if (string.IsNullOrEmpty(username))
      return new GetUserDtoResponse {Status = GetUserResponseStatus.UsernameEmpty};

    var user = await _dbUserManagement.GetUser(username);
    return new GetUserDtoResponse {Status = user.Status, User = UserDto.FromUser(user.User)};
  }


  public async Task<List<UserDto>> ListUsers()
  {
    var result = await _dbUserManagement.ListUsers();
    return result.Select(UserDto.FromUser).Where(x => x is not null).Select(x => x!).ToList();
  }

  public async Task<UpdatePasswordResponse> UpdatePassword(string username, string oldPassword,
    string newPassword)
  {
    if (string.IsNullOrEmpty(username) || string.IsNullOrEmpty(oldPassword) ||
        string.IsNullOrEmpty(newPassword))
      return new UpdatePasswordResponse {Status = UpdatePasswordResponseStatus.InputEmpty};

    var user = await _dbUserManagement.GetUser(username);
    if (user.Status != GetUserResponseStatus.Ok || user.User is null)
      return new UpdatePasswordResponse {Status = UpdatePasswordResponseStatus.UserNotFound};

    var oldPasswordHash = CreatePasswordHash(oldPassword, user.User.Salt);
    if (oldPasswordHash != user.User.PasswordHash)
      return new UpdatePasswordResponse
      {
        Status = UpdatePasswordResponseStatus.OldPasswordIncorrect
      };

    var newPasswordHash = CreatePasswordHash(newPassword, user.User.Salt);
    user.User.PasswordHash = newPasswordHash;
    var updatedUser = await _dbUserManagement.UpdateUser(user.User);
    if (updatedUser.Status != EditUserResponseStatus.Ok)
      return new UpdatePasswordResponse {Status = UpdatePasswordResponseStatus.InternalError};

    return new UpdatePasswordResponse
    {
      Status = UpdatePasswordResponseStatus.Ok, User = updatedUser.User,
    };
  }

  public async Task<UpdatePasswordResponse> ResetPassword(string username, string password)
  {
    if (string.IsNullOrEmpty(username) || string.IsNullOrEmpty(password))
      return new UpdatePasswordResponse {Status = UpdatePasswordResponseStatus.InputEmpty};

    var user = await _dbUserManagement.GetUser(username);
    if (user.Status != GetUserResponseStatus.Ok || user.User is null)
      return new UpdatePasswordResponse {Status = UpdatePasswordResponseStatus.UserNotFound};

    var newPasswordHash = CreatePasswordHash(password, user.User.Salt);
    user.User.PasswordHash = newPasswordHash;
    var updatedUser = await _dbUserManagement.UpdateUser(user.User);
    if (updatedUser.Status != EditUserResponseStatus.Ok)
      return new UpdatePasswordResponse {Status = UpdatePasswordResponseStatus.InternalError};

    return new UpdatePasswordResponse
    {
      Status = UpdatePasswordResponseStatus.Ok, User = updatedUser.User,
    };
  }
}
