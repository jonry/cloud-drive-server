namespace CloudDriveServer.Constants;

public static class Claims
{
  public static class Account
  {
    public const string Owner = "owner";
    public const string Admin = "admin";
    public const string User = "user";
    public const string OwnerAdmin = "ownerAndAdmin";
    public const string OwnerAdminUser = "ownerAdminUser";
  }
}
