using System.Security.Principal;
using CloudDriveServer.Interfaces;
using CloudDriveServer.Models.Files.Requests;
using CloudDriveServer.Models.Files.Responses;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MimeKit;

namespace CloudDriveServer.Controllers;

[ApiController, Route("api/[controller]"), Authorize]
public class FileController : ControllerBase
{
  private readonly IFileService _fileService;

  public FileController(IFileService fileService)
  {
    _fileService = fileService;
  }

  private static bool GetUsername(IPrincipal user, out string? username)
  {
    username = "";
    var userIdentity = user.Identity;
    if (userIdentity is null)
      return false;

    username = userIdentity.Name;
    return !string.IsNullOrEmpty(username);
  }

  [HttpPost("list")]
  public IActionResult ListDirectory(ListFilesRequest request)
  {
    var success = GetUsername(User, out var username);
    if (!success || string.IsNullOrEmpty(username))
      return BadRequest("user-not-signed-in");

    request.Username = username;

    var result = _fileService.ListFiles(request);
    return result.Status switch
    {
      ListFilesResponseStatus.Ok => Ok(result),
      ListFilesResponseStatus.NoUserProvided => new BadRequestObjectResult("user-is-empty"),
      ListFilesResponseStatus.PathInvalid => new BadRequestObjectResult("path-invalid"),
      ListFilesResponseStatus.DirectoryDoesNotExist => new BadRequestObjectResult(
        "directory-does-not-exist"),
      ListFilesResponseStatus.ListingError => new BadRequestObjectResult(result.Message),
      _ => new BadRequestResult()
    };
  }

  [HttpPost("mkdir")]
  public IActionResult MakeDirectory(MakeDirectoryRequest request)
  {
    var success = GetUsername(User, out var username);
    if (!success || string.IsNullOrEmpty(username))
      return BadRequest("user-not-signed-in");

    request.Username = username;

    var result = _fileService.MakeDirectory(request);
    return result.Status switch
    {
      MakeDirectoryResponseStatus.Ok => Ok(),
      MakeDirectoryResponseStatus.NoUserProvided => new BadRequestObjectResult("user-is-empty"),
      MakeDirectoryResponseStatus.PathInvalid => new BadRequestObjectResult("path-invalid"),
      MakeDirectoryResponseStatus.CreationFailed => new BadRequestObjectResult(result.Message),
      _ => new BadRequestResult()
    };
  }

  [HttpPost("upload")]
  public async Task<IActionResult> UploadFile()
  {
    var success = GetUsername(User, out var username);
    if (!success || string.IsNullOrEmpty(username))
      return BadRequest("user-not-signed-in");

    var files = Request.Form.Files;

    var request = new UploadFileRequest {Username = username, Files = files.ToList()};

    var result = await _fileService.UploadFiles(request);
    return result.Status switch
    {
      UploadFileResponseStatus.Ok => Ok(result),
      UploadFileResponseStatus.NoUserProvided => BadRequest("user-is-empty"),
      UploadFileResponseStatus.FilenameInvalid => BadRequest("filename-invalid"),
      UploadFileResponseStatus.NoFilesProvided => BadRequest("no-files-provided"),
      UploadFileResponseStatus.UploadFailed => BadRequest(result.Message),
      _ => BadRequest()
    };
  }

  [HttpPost("download")]
  public IActionResult DownloadFile(DownloadFileRequest request)
  {
    var success = GetUsername(User, out var username);
    if (!success || string.IsNullOrEmpty(username))
      return BadRequest("user-not-signed-in");
    request.Username = username;

    var response = _fileService.DownloadFile(request);

    return response.Status switch
    {
      DownloadFileResponseStatus.Ok => PhysicalFile(response.FilePath,
        MimeTypes.GetMimeType(response.FilePath),
        Path.GetFileName(response.FilePath)),
      DownloadFileResponseStatus.OkArchive => HandleArchiveDownload(response),
      DownloadFileResponseStatus.NoUserProvided => BadRequest("user-is-empty"),
      DownloadFileResponseStatus.FilenameInvalid => BadRequest("filename-invalid"),
      DownloadFileResponseStatus.FileNotFound => BadRequest("file-not-found"),
      _ => BadRequest()
    };
  }

  private IActionResult HandleArchiveDownload(DownloadFileResponse response)
  {
    var fs = new FileStream(response.FilePath, FileMode.Open, FileAccess.Read, FileShare.Read, 4096,
      FileOptions.DeleteOnClose);
    fs.Position = 0;

    return File(fs, MimeTypes.GetMimeType(response.FilePath));
  }
}
