using CloudDriveServer.Constants;
using CloudDriveServer.Interfaces;
using CloudDriveServer.Models.UserManagement.Requests;
using CloudDriveServer.Models.UserManagement.Responses;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace CloudDriveServer.Controllers;

[ApiController, Route("api/[controller]"), Authorize]
public class AccountController : ControllerBase
{
  private readonly IAccountService _accountService;

  public AccountController(IAccountService accountService)
  {
    _accountService = accountService;
  }

  [HttpPost("signin"), AllowAnonymous]
  public async Task<IActionResult> SignIn(SignInRequest request)
  {
    if (string.IsNullOrEmpty(request.Username) || string.IsNullOrEmpty(request.Password))
      return new BadRequestObjectResult("username-or-password-empty");


    var result = await _accountService.SignIn(request.Username, request.Password);
    return result.Status switch
    {
      SignInResponseStatus.Ok => Ok(result),
      SignInResponseStatus.UserNotFound => new NotFoundObjectResult("user-does-not-exist"),
      SignInResponseStatus.WrongPassword => new BadRequestObjectResult("wrong-password"),
      SignInResponseStatus.UsernameOrPasswordEmpty => new BadRequestObjectResult(
        "no-username-or-password"),
      _ => new BadRequestResult()
    };
  }

  [HttpPost("refresh"), AllowAnonymous]
  public async Task<IActionResult> RefreshSignIn(RefreshSignInRequest request)
  {
    if (string.IsNullOrEmpty(request.Username) || string.IsNullOrEmpty(request.RefreshToken))
      return new BadRequestObjectResult("username-or-token-empty");

    var result = await _accountService.RefreshSignIn(request.Username, request.RefreshToken);
    return result.Status switch
    {
      RefreshSignInResponseStatus.Ok => Ok(result),
      RefreshSignInResponseStatus.UserNotFound => new BadRequestObjectResult("user-not-found"),
      RefreshSignInResponseStatus.TokenNotFound => new BadRequestObjectResult("token-not-found"),
      RefreshSignInResponseStatus.TokenExpired => new BadRequestObjectResult("token-expired"),
      _ => new BadRequestResult()
    };
  }

  [HttpPost("create"), Authorize(Claims.Account.OwnerAdmin)]
  public async Task<IActionResult> CreateUser(CreateUserRequest createUserRequest)
  {
    if (string.IsNullOrEmpty(createUserRequest.Username) ||
        string.IsNullOrEmpty(createUserRequest.Password))
    {
      return new BadRequestObjectResult("username-or-password-empty");
    }

    var result = await _accountService.CreateUser(createUserRequest);
    return result.Status switch
    {
      CreateUserResponseStatus.Ok => Ok(result),
      CreateUserResponseStatus.UserAlreadyExists => new BadRequestObjectResult(
        "user-already-exists"),
      CreateUserResponseStatus.UsernameNotProvided => new BadRequestObjectResult("username-empty"),
      CreateUserResponseStatus.PasswordNotProvided => new BadRequestObjectResult("password-empty"),
      CreateUserResponseStatus.UsernameNotAlphaNumeric => new BadRequestObjectResult(
        "username-not-alphanumeric"),
      CreateUserResponseStatus.HomeDirectoryNotCreated =>
        new BadRequestObjectResult(result.Message),
      _ => new BadRequestResult()
    };
  }

  [HttpPost("edit"), Authorize(Claims.Account.OwnerAdmin)]
  public async Task<IActionResult> EditUser(EditUserRequest editUserRequest)
  {
    if (string.IsNullOrEmpty(editUserRequest.Name))
    {
      return new BadRequestObjectResult("no-username-specified");
    }

    var result = await _accountService.EditUser(editUserRequest);
    return result.Status switch
    {
      EditUserResponseStatus.Ok => Ok(result),
      EditUserResponseStatus.NameEmpty => new BadRequestObjectResult("username-empty"),
      EditUserResponseStatus.CantChangeUsername => new BadRequestObjectResult(
        "username-cant-be-changed"),
      EditUserResponseStatus.UserNotFound => new NotFoundObjectResult("user-not-found"),
      _ => new BadRequestResult()
    };
  }

  [HttpDelete("delete"), Authorize(Claims.Account.OwnerAdmin)]
  public async Task<IActionResult> DeleteUser(DeleteUserRequest deleteUserRequest)
  {
    if (string.IsNullOrEmpty(deleteUserRequest.Username))
    {
      return new BadRequestObjectResult("username-empty");
    }

    var identity = User.Identity;
    if (identity is null)
      return new BadRequestObjectResult("cant-read-signed-in-user");

    var signedInUser = identity.Name;
    if (string.IsNullOrEmpty(signedInUser))
      return new BadRequestObjectResult("username-of-signed-in-user-is-empty");

    if (signedInUser == deleteUserRequest.Username)
      return new BadRequestObjectResult("you-cant-delete-yourself");

    var result = await _accountService.DeleteUser(deleteUserRequest.Username);
    return result.Status switch
    {
      DeleteUserResponseStatus.Ok => Ok(result),
      DeleteUserResponseStatus.UserNotFound => new NotFoundObjectResult("user-not-found"),
      DeleteUserResponseStatus.CantDeleteYourself => new BadRequestObjectResult(
        "you-cant-delete-yourself"),
      DeleteUserResponseStatus.CouldNotDeleteHomeDirectory => new BadRequestObjectResult(
        result.Message),
      _ => new BadRequestResult()
    };
  }

  [HttpGet("get/{username}"), Authorize(Claims.Account.OwnerAdmin)]
  public async Task<IActionResult> GetUser(string username)
  {
    if (string.IsNullOrEmpty(username))
      return new BadRequestObjectResult("username-empty");

    var result = await _accountService.GetUser(username);
    return result.Status switch
    {
      GetUserResponseStatus.Ok => Ok(result),
      GetUserResponseStatus.UsernameEmpty => new BadRequestObjectResult("username-empty"),
      GetUserResponseStatus.UserNotFound => new NotFoundObjectResult("user-not-found"),
      _ => new BadRequestResult()
    };
  }

  [HttpGet("getMySelf"), Authorize(Claims.Account.OwnerAdminUser)]
  public async Task<IActionResult> GetMySelf()
  {
    if (User.Identity is null)
    {
      return new BadRequestObjectResult("could-not-get-your-username");
    }

    var username = User.Identity.Name;

    if (string.IsNullOrEmpty(username))
    {
      return new BadRequestObjectResult("username-is-empty");
    }

    var result = await _accountService.GetUser(username);
    return result.Status switch
    {
      GetUserResponseStatus.Ok => Ok(result.User),
      GetUserResponseStatus.UsernameEmpty => new BadRequestObjectResult("username-is-empty"),
      GetUserResponseStatus.UserNotFound => new NotFoundObjectResult("user-not-found"),
      _ => new BadRequestResult(),
    };
  }

  [HttpGet("list"), Authorize(Claims.Account.OwnerAdmin)]
  public async Task<IActionResult> ListUsers()
  {
    var result = await _accountService.ListUsers();
    return Ok(result);
  }
}
