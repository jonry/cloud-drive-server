using CloudDriveServer.Models.Files.Requests;
using CloudDriveServer.Models.Files.Responses;

namespace CloudDriveServer.Interfaces;

public interface IFileService
{
  public ListFilesResponse ListFiles(ListFilesRequest request);
  public MakeDirectoryResponse MakeDirectory(MakeDirectoryRequest request);
  public Task<UploadFileResponse> UploadFiles(UploadFileRequest request);
  public DownloadFileResponse DownloadFile(DownloadFileRequest request);
}
