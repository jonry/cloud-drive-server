namespace CloudDriveServer.Interfaces.Database;

public interface IDbInitService
{
  public Task InitDatabase();
}
