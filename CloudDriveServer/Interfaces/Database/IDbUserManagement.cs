using CloudDriveServer.Models.Database.Responses;
using CloudDriveServer.Models.UserManagement;
using CloudDriveServer.Models.UserManagement.Responses;

namespace CloudDriveServer.Interfaces.Database;

public interface IDbUserManagement
{
  public Task InitEmptyDb();
  public Task<CreateUserResponse> CreateUser(User user);
  public Task<EditUserResponse> UpdateUser(User user);
  public Task<DeleteUserResponse> DeleteUser(string username);
  public Task<GetUserResponse> GetUser(string username);
  public Task<List<User>> ListUsers();
  public Task<AddRefreshTokenResponse> AddRefreshTokenToUser(string username, RefreshToken refreshToken);
  public Task<CheckRefreshTokenResponse> CheckRefreshToken(string username, string token);
  public Task SetLastSignInOnForUser(string username, DateTime lastSignIn);
}
