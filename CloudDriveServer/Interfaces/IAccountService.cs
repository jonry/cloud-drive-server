using CloudDriveServer.Models.UserManagement;
using CloudDriveServer.Models.UserManagement.Requests;
using CloudDriveServer.Models.UserManagement.Responses;

namespace CloudDriveServer.Interfaces;

public interface IAccountService
{
  public Task<CreateUserResponse> CreateUser(CreateUserRequest user);
  public Task<SignInResponse> SignIn(string username, string password);
  public Task<RefreshSignInResponse> RefreshSignIn(string username, string refreshToken);
  public Task<EditUserResponse> EditUser(EditUserRequest editUserRequest);
  public Task<DeleteUserResponse> DeleteUser(string username);
  public Task<GetUserDtoResponse> GetUser(string username);
  public Task<List<UserDto>> ListUsers();
  public Task<UpdatePasswordResponse> UpdatePassword(string username, string oldPassword, string newPassword);
  public Task<UpdatePasswordResponse> ResetPassword(string username, string password);
}
