namespace CloudDriveServer.Models.Database.Responses;

public class CheckRefreshTokenResponse
{
  public CheckRefreshTokenResponseStatus Status { get; set; }
}

public enum CheckRefreshTokenResponseStatus
{
  Ok,
  UserNotFound,
  TokenNotFound,
  TokenExpired
}
