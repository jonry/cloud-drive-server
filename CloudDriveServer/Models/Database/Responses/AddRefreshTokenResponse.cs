namespace CloudDriveServer.Models.Database.Responses;

public class AddRefreshTokenResponse
{
  public AddRefreshTokenResponseStatus Status { get; set; }
}

public enum AddRefreshTokenResponseStatus
{
  Ok,
  UserNotFound
}
