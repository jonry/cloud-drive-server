namespace CloudDriveServer.Models.Files;

public class FileDto
{
  public string Name { get; set; } = "";
  public FileType Type { get; set; }
  public uint Size { get; set; }
  public DateTime LastModifiedOn { get; set; }
}

public enum FileType
{
  File,
  Directory
}
