namespace CloudDriveServer.Models.Files.Requests;

public class UploadFileRequest
{
  public string Username { get; set; } = "";
  public List<IFormFile> Files { get; init; } = new();
}
