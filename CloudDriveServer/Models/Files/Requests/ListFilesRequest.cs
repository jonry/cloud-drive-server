using Newtonsoft.Json;

namespace CloudDriveServer.Models.Files.Requests;

public class ListFilesRequest
{
  public string Username { get; set; } = "";
  public string Path { get; set; } = "";
}
