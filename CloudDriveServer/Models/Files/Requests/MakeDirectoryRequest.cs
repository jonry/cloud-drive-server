namespace CloudDriveServer.Models.Files.Requests;

public class MakeDirectoryRequest
{
  public string Username { get; set; } = "";
  public string Path { get; set; } = "";
}
