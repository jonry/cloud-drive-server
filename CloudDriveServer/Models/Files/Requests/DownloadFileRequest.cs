namespace CloudDriveServer.Models.Files.Requests;

public class DownloadFileRequest
{
  public string Username { get; set; } = "";
  public string Path { get; set; } = "";
}
