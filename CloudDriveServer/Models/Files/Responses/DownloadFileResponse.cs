namespace CloudDriveServer.Models.Files.Responses;

public class DownloadFileResponse
{
  public DownloadFileResponseStatus Status { get; init; }
  public string FilePath { get; init; } = "";
}

public enum DownloadFileResponseStatus
{
  Ok,
  OkArchive,
  NoUserProvided,
  FilenameInvalid,
  FileNotFound
}
