namespace CloudDriveServer.Models.Files.Responses;

public class UploadFileResponse
{
  public UploadFileResponseStatus Status { get; init; }
  public List<string> UploadedFiles { get; init; } = new();
  public string Message { get; init; } = "";
}

public enum UploadFileResponseStatus
{
  Ok,
  NoUserProvided,
  NoFilesProvided,
  FilenameInvalid,
  UploadFailed
}
