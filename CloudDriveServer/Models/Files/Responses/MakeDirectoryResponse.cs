namespace CloudDriveServer.Models.Files.Responses;

public class MakeDirectoryResponse
{
  public MakeDirectoryResponseStatus Status { get; init; }
  public string Message { get; init; } = "";
}

public enum MakeDirectoryResponseStatus
{
  Ok,
  NoUserProvided,
  PathInvalid,
  CreationFailed
}
