using Newtonsoft.Json;

namespace CloudDriveServer.Models.Files.Responses;

public class ListFilesResponse
{
  public ListFilesResponseStatus Status { get; init; }
  public List<FileDto> Contents { get; init; } = new();
  public string Message { get; init; } = "";
}

public enum ListFilesResponseStatus
{
  Ok,
  NoUserProvided,
  PathInvalid,
  DirectoryDoesNotExist,
  ListingError
}
