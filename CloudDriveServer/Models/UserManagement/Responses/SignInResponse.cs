using Newtonsoft.Json;

namespace CloudDriveServer.Models.UserManagement.Responses;

public class SignInResponse
{
  public SignInResponseStatus Status { get; set; }
  [JsonProperty("jwt")] public string? Jwt { get; set; }
  [JsonProperty("refreshToken")] public string? RefreshToken { get; set; }
}

public enum SignInResponseStatus
{
  Ok,
  UserNotFound,
  WrongPassword,
  UsernameOrPasswordEmpty
}
