namespace CloudDriveServer.Models.UserManagement.Responses;

public class CreateUserResponse
{
  public CreateUserResponseStatus Status { get; set; }
  public User? User { get; set; }
  public string? Message { get; set; }
}

public enum CreateUserResponseStatus
{
  Ok,
  UserAlreadyExists,
  UsernameNotProvided,
  UsernameNotAlphaNumeric,
  PasswordNotProvided,
  HomeDirectoryNotCreated
}
