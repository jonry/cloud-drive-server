namespace CloudDriveServer.Models.UserManagement.Responses;

public class DeleteUserResponse
{
  public DeleteUserResponseStatus Status { get; set; }
  public string? Message { get; set; }
}

public enum DeleteUserResponseStatus
{
  Ok,
  UserNotFound,
  CantDeleteYourself,
  CouldNotDeleteHomeDirectory
}
