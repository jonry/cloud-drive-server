using Newtonsoft.Json;

namespace CloudDriveServer.Models.UserManagement.Responses;

public class RefreshSignInResponse
{
  public RefreshSignInResponseStatus Status { get; set; }
  [JsonProperty("jwt")] public string? Jwt { get; set; }
  [JsonProperty("refreshToken")] public string? RefreshToken { get; set; }
}

public enum RefreshSignInResponseStatus
{
  Ok,
  UserNotFound,
  TokenNotFound,
  TokenExpired
}
