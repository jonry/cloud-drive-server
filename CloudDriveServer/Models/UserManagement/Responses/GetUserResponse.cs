namespace CloudDriveServer.Models.UserManagement.Responses;

public class GetUserResponse
{
  public GetUserResponseStatus Status { get; init; }
  public User? User { get; init; }
}

public enum GetUserResponseStatus
{
  Ok,
  UsernameEmpty,
  UserNotFound
}
