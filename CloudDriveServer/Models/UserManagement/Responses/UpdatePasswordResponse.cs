namespace CloudDriveServer.Models.UserManagement.Responses;

public class UpdatePasswordResponse
{
  public UpdatePasswordResponseStatus Status { get; set; }
  public User? User { get; set; }
}

public enum UpdatePasswordResponseStatus
{
  Ok,
  InputEmpty,
  UserNotFound,
  OldPasswordIncorrect,
  InternalError
}
