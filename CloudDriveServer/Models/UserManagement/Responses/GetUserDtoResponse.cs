namespace CloudDriveServer.Models.UserManagement.Responses;

public class GetUserDtoResponse
{
  public GetUserResponseStatus Status { get; init; }
  public UserDto? User { get; init; }
}
