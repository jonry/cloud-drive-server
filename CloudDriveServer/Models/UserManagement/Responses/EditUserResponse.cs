namespace CloudDriveServer.Models.UserManagement.Responses;

public class EditUserResponse
{
  public EditUserResponseStatus Status { get; set; }
  public User? User { get; set; }
}

public enum EditUserResponseStatus
{
  Ok,
  NameEmpty,
  CantChangeUsername,
  UserNotFound
}
