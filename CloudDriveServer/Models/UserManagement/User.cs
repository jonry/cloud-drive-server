using Newtonsoft.Json;

namespace CloudDriveServer.Models.UserManagement;

public class User
{
  public User(DateTime createdOn, DateTime modifiedOn, string name, string? email, string passwordHash, string salt,
      int quota, Role role, List<RefreshToken>? refreshTokens = null)
  {
    CreatedOn = createdOn;
    ModifiedOn = modifiedOn;
    LastSignInOn = DateTime.Now;
    Name = name;
    Email = email;
    PasswordHash = passwordHash;
    Salt = salt;
    Quota = quota;
    Role = role;
    RefreshTokens = refreshTokens ?? new List<RefreshToken>();
  }

  [JsonProperty("createdOn")] public DateTime CreatedOn { get; set; }
  [JsonProperty("modifiedOn")] public DateTime ModifiedOn { get; set; }
  [JsonProperty("lastSignInOn")] public DateTime LastSignInOn { get; set; }
  [JsonProperty("name")] public string Name { get; set; }
  [JsonProperty("email")] public string? Email { get; set; }
  [JsonProperty("passwordHash")] public string PasswordHash { get; set; }
  [JsonProperty("salt")] public string Salt { get; set; }
  [JsonProperty("quota")] public int Quota { get; set; }
  [JsonProperty("role")] public Role Role { get; set; }
  [JsonProperty("refreshTokens")] public List<RefreshToken> RefreshTokens { get; set; }

  public static string RoleToString(Role role)
  {
    return role switch
    {
      Role.Owner => "owner",
      Role.Admin => "admin",
      Role.User => "user",
      Role.Unknown => "unknown",
      _ => "unknown"
    };
  }

  public static Role StringToRole(string? role)
  {
    return role switch
    {
      "owner" => Role.Owner,
      "admin" => Role.Admin,
      "user" => Role.User,
      _ => Role.Unknown
    };
  }
}

public class RefreshToken
{
  public string? Token { get; set; }
  public DateTime ValidTo { get; set; }
}

public enum Role
{
  Owner,
  Admin,
  User,
  Unknown
}
