namespace CloudDriveServer.Models.UserManagement.Requests;

public class EditUserRequest
{
  public string? Name { get; set; }
  public string? Email { get; set; }
  public int? Quota { get; set; }
  public string? Role { get; set; }
}
