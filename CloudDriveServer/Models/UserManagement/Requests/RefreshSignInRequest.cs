using Newtonsoft.Json;

namespace CloudDriveServer.Models.UserManagement.Requests;

public class RefreshSignInRequest
{
  [JsonProperty("username")] public string Username { get; set; } = "";
  [JsonProperty("refreshToken")] public string RefreshToken { get; set; } = "";
}
