using Newtonsoft.Json;

namespace CloudDriveServer.Models.UserManagement.Requests;

public class CreateUserRequest
{
  [JsonProperty("username")] public string? Username { get; set; }
  [JsonProperty("email")] public string? Email { get; set; }
  [JsonProperty("password")] public string? Password { get; set; }
  [JsonProperty("quota")] public int? Quota { get; set; }
  [JsonProperty("role")] public string? Role { get; set; }
}
