using Newtonsoft.Json;

namespace CloudDriveServer.Models.UserManagement.Requests;

public class SignInRequest
{
  [JsonProperty("username")]
  public string? Username { get; set; }
  [JsonProperty("password")]
  public string? Password { get; set; }
}
