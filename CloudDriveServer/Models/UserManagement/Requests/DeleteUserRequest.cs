namespace CloudDriveServer.Models.UserManagement.Requests;

public class DeleteUserRequest
{
  public string? Username { get; set; }
}
