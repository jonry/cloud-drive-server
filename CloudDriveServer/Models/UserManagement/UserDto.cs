using Newtonsoft.Json;

namespace CloudDriveServer.Models.UserManagement;

public class UserDto
{
  [JsonProperty("name")] public string Name { get; set; }
  [JsonProperty("email")] public string? Email { get; set; }
  [JsonProperty("quota")] public int Quota { get; set; }
  [JsonProperty("role")] public Role Role { get; set; }

  public UserDto(string name, string? email, int quota, Role role)
  {
    Name = name;
    Email = email;
    Quota = quota;
    Role = role;
  }

  public static UserDto? FromUser(User? user)
  {
    if (user is null)
      return null;

    return new UserDto
    (
        user.Name,
        user.Email,
        user.Quota,
        user.Role
    );
  }
}
