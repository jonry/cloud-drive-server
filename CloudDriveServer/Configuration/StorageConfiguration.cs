namespace CloudDriveServer.Configuration;

public class StorageConfiguration
{
  public string BasePath { get; set; } = "/tmp";
}
