namespace CloudDriveServer.Configuration;

public class JwtConfiguration
{
  public string Secret { get; set; } = "";
  public string Issuer { get; set; } = "cloud-drive";
  public string Audience { get; set; } = "cloud-drive";
  public int ExpirationInMinutes { get; set; } = 15;
}
