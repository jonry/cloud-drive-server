using System.Security.Claims;
using System.Text;
using CloudDriveServer.Configuration;
using CloudDriveServer.Constants;
using CloudDriveServer.Interfaces;
using CloudDriveServer.Interfaces.Database;
using CloudDriveServer.Services;
using CloudDriveServer.Services.Database;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using Serilog;

var builder = WebApplication.CreateBuilder(args);

builder.Host.UseSerilog((context, serviceProvider, loggerConfig) =>
  loggerConfig.ReadFrom.Configuration(builder.Configuration)
);
// add configuration
builder.Services
  .Configure<JwtConfiguration>(builder.Configuration.GetSection("JwtConfiguration"))
  .Configure<StorageConfiguration>(builder.Configuration.GetSection("StorageConfiguration"));

// add services
builder.Services
  .AddSingleton<IDbInitService, MemoryDbInitService>()
  .AddSingleton<IDbUserManagement, InMemoryUserDatabase>()
  .AddTransient<IAccountService, AccountService>()
  .AddTransient<IFileService, FileService>();

builder.Services.AddControllers();

builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();


builder.Services.AddAuthorization(options =>
{
  options.AddPolicy(JwtBearerDefaults.AuthenticationScheme, policy =>
  {
    policy.AddAuthenticationSchemes(JwtBearerDefaults.AuthenticationScheme);
    policy.RequireAuthenticatedUser();
  });
  options.AddPolicy(Claims.Account.Owner, policy =>
    policy.RequireClaim(ClaimTypes.Role, Claims.Account.Owner));
  options.AddPolicy(Claims.Account.Admin, policy =>
    policy.RequireClaim(ClaimTypes.Role, Claims.Account.Admin));
  options.AddPolicy(Claims.Account.User, policy =>
    policy.RequireClaim(ClaimTypes.Role, Claims.Account.User));
  options.AddPolicy(Claims.Account.OwnerAdmin, policy =>
    policy.RequireClaim(ClaimTypes.Role, Claims.Account.Owner, Claims.Account.Admin));
  options.AddPolicy(Claims.Account.OwnerAdminUser, policy =>
    policy.RequireClaim(ClaimTypes.Role, Claims.Account.Owner, Claims.Account.Admin,
      Claims.Account.User));
});

builder.Services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
  .AddJwtBearer(options =>
  {
    var jwtSettings = builder.Configuration.GetSection("JwtConfiguration").Get<JwtConfiguration>();
    if (jwtSettings is null || string.IsNullOrEmpty(jwtSettings.Issuer) ||
        string.IsNullOrEmpty(jwtSettings.Secret))
    {
      throw new ArgumentNullException(nameof(jwtSettings), "no jwt settings found");
    }

    options.TokenValidationParameters = new TokenValidationParameters
    {
      ValidateAudience = true,
      ValidateIssuer = true,
      ValidateLifetime = true,
      ValidateIssuerSigningKey = true,
      ValidIssuer = jwtSettings.Issuer,
      ValidAudience = jwtSettings.Audience,
      IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(jwtSettings.Secret))
    };
  });


var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
  app.UseSwagger();
  app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthentication();
app.UseAuthorization();

app.MapControllers();

await InitDatabase(app);

app.Run();


async Task InitDatabase(IHost webApp)
{
  var initService = webApp.Services.GetService<IDbInitService>();
  if (initService is null)
  {
    throw new ArgumentNullException(nameof(initService), "could not get init service");
  }

  await initService.InitDatabase();
}
