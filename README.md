# Cloud Drive Server

Host your own cloud with web interface and client sync.
This is the backend application of the project.


## Getting started
Run with `dotnet run`.
Use the `Dockerfile` for containerization.


## Features
- User Management
- File Browsing, Download and Upload
- (more to come)


## Support
For support open issues in this repo.

## Roadmap
- Richer file api
- Sync API for desktop clients

## Contributing
Contributions are welcomed!

## Authors and acknowledgment
- https://gitlab.com/jonry

## License
MIT License

## Project status
Status: Active

Working on it during free time.
